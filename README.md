# Anepokis License

* Homepage: [salifm.github.io/anepokis-license](https://salifm.github.io/anepokis-license)
* Repository: [github.com/salifm/anepokis-license](https://github.com/salifm/anepokis-license)

### Text of the Anepokis License v. 1.0:

* Plain text: [LICENSE.txt](./LICENSE.txt)
* Web page: [LICENSE.html](./LICENSE.html)
